//
//  Message.swift
//  Flash Chat iOS13
//
//  Created by Sergio Escalante Ordonez on 25/09/2020.
//  Copyright © 2020 Angela Yu. All rights reserved.
//

import Foundation

struct Message {
    let sender: String
    let body: String
}
