//
//  RegisterViewController.swift
//  Flash Chat iOS13
//
//  Created by Angela Yu on 21/10/2019.
//  Copyright © 2019 Angela Yu. All rights reserved.
//

import UIKit
import Firebase

class RegisterViewController: UIViewController {

    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var errorView: UIView!
    
    @IBAction func registerPressed(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if let email = emailTextfield.text, let password = passwordTextfield.text {
            Auth.auth().createUser(withEmail: email, password: password) { [weak self] (authResult, error) in
                if let e = error {
                    self?.errorLabel.text = e.localizedDescription
                    self?.errorView.isHidden = false
                } else {
                    self?.errorView.isHidden = true
                    self?.performSegue(withIdentifier: K.registerSegue, sender: self)
                }
            }
        }
    }
    
}
